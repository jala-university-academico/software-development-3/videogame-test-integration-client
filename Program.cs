
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using System.Text;
using VideoGamesAPI.BusinessLogic;
using VideoGamesAPI.BusinessLogic.service;
using VideoGamesAPI.DataAccess;
using VideoGamesAPI.DataAccess.mapper;
using VideoGamesAPI.DataAccess.repositorty;

namespace VideoGamesAPI;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.
        builder.Services.AddControllers();
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddSwaggerGen(options =>
        {
            options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
            {
                In = ParameterLocation.Header,
                Name = "Authorization",
                Type = SecuritySchemeType.ApiKey
            });

            options.OperationFilter<SecurityRequirementsOperationFilter>();
        });

        builder.Services.AddEndpointsApiExplorer();

        builder.Services.AddDbContext<GameDbContext>(options =>
        {
            options.UseInMemoryDatabase("GameDatabase");
        });

        builder.Services.AddAutoMapper(typeof(GameMapping));
        builder.Services.AddScoped<IGameRepository, GameRepository>();
        builder.Services.AddScoped<IGamesService, GamesService>();
        builder.Services.AddScoped<IUserRepository, UserRepository>();
        builder.Services.AddScoped<IUserService, UserService>();

        builder.Services.AddSingleton<JwtUtil>(provider =>
        {
            var configuration = provider.GetRequiredService<IConfiguration>();
            var issuer = configuration["Jwt:Issuer"];
            var secretKey = configuration["Jwt:Key"];
            var roleClaimType = "role";
            return new JwtUtil(issuer, secretKey, roleClaimType);
        });


        builder.Services.AddScoped<IAuthService, AuthService>();

        builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                        builder.Configuration["Jwt:Key"]))
                };
            });

        var app = builder.Build();

        // Configure the HTTP request pipeline.
        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseHttpsRedirection();

        app.UseAuthorization();

        app.MapControllers();

        app.Run();
    }
}
