﻿using System.ComponentModel.DataAnnotations;

namespace VideoGamesAPI.BusinessLogic.dto;

public class RegisterDto
{
    [Required(ErrorMessage = "Username is required")]
    [StringLength(50, ErrorMessage = "Username must be between 1 and 50 characters", MinimumLength = 1)]
    public string Username { get; set; }

    [Required(ErrorMessage = "Password is required")]
    [MinLength(8, ErrorMessage = "Password must be at least 8 characters long")]
    [RegularExpression(@"^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9]+)$", ErrorMessage = "Password must contain at least one number")]
    public string Password { get; set; }

    [Required(ErrorMessage = "Role is required")]
    [RegularExpression("^(User|Admin)$", ErrorMessage = "Role must be either 'User' or 'Admin'")]
    public string Role { get; set; }
}
