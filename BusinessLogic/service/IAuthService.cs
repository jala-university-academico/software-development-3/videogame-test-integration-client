﻿using VideoGamesAPI.BusinessLogic.dto;

namespace VideoGamesAPI.BusinessLogic.service;

public interface IAuthService
{
    string Login(LoginDto loginDto);
    void Register(RegisterDto registerDto);
}