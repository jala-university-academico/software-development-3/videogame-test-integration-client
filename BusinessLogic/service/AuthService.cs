﻿namespace VideoGamesAPI.BusinessLogic.service; 
using VideoGamesAPI.BusinessLogic.dto;
using VideoGamesAPI.DataAccess.repositorty;

/// <summary>
/// Interfaz que representa el servicio de autenticación.
/// </summary>
public class AuthService : IAuthService
{
    private readonly IUserRepository _userRepository;
    private readonly JwtUtil _jwtUtil;

    public AuthService(IUserRepository userRepository, JwtUtil jwtUtil)
    {
        _userRepository = userRepository;
        _jwtUtil = jwtUtil;
    }

    public string Login(LoginDto loginDto)
    {
        var user = _userRepository.GetUserByUsernameAndPassword(loginDto.Username, loginDto.Password);
        if (user == null)
        {
            return null;
        }

        return _jwtUtil.Create(user.Username, user.Role);
    }

    public void Register(RegisterDto registerDto)
    {
        var existingUser = _userRepository.GetUserByUsername(registerDto.Username);
        if (existingUser != null)
        {
            throw new InvalidOperationException("Username already exists.");
        }

        string hashedPassword = HashPassword(registerDto.Password);
        registerDto.Password = hashedPassword;

        _userRepository.AddUser(registerDto);
    }

    private string HashPassword(string password)
    {
        string hashedPassword = BCrypt.Net.BCrypt.HashPassword(password);
        return hashedPassword;
    }
}