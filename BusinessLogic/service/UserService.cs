﻿using VideoGamesAPI.BusinessLogic.dto;
using VideoGamesAPI.DataAccess.repositorty;

namespace VideoGamesAPI.BusinessLogic.service;

public class UserService : IUserService
{
    private readonly IUserRepository _userRepository;

    public UserService(IUserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public IEnumerable<UserDto> GetUsers()
    {
       return _userRepository.GetUsers();
    }
}