﻿using AutoMapper;
using VideoGamesAPI.BusinessLogic.dto;
using VideoGamesAPI.DataAccess.model;

namespace VideoGamesAPI.DataAccess.repositorty;
public class UserRepository : IUserRepository
{
    private readonly GameDbContext _dbContext;
    private readonly IMapper _mapper;

    public UserRepository(GameDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public IEnumerable<UserDto> GetUsers()
    {
        var users = _dbContext.Users.ToList();
        return _mapper.Map<List<UserDto>>(users);
    }

    public UserDto AddUser(RegisterDto userDto)
    {
        var user = _mapper.Map<UserEntity>(userDto);
        _dbContext.Users.Add(user);
        _dbContext.SaveChanges();
        return _mapper.Map<UserDto>(user);
    }

    public UserDto GetUserByUsernameAndPassword(string username, string password)
    {
        var user = _dbContext.Users.FirstOrDefault(u => u.Username == username);
        if (user == null)
        {
            return null;
        }
        bool passwordMatches = BCrypt.Net.BCrypt.Verify(password, user.Password);
        return passwordMatches ? _mapper.Map<UserDto>(user) : null;
    }

    public UserDto GetUserByUsername(string username)
    {
        var user = _dbContext.Users.FirstOrDefault(u => u.Username == username);
        return _mapper.Map<UserDto>(user);
    }
}