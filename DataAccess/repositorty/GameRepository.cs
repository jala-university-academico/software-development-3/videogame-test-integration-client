﻿namespace VideoGamesAPI.DataAccess.repositorty;

using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using VideoGamesAPI.BusinessLogic.dto;
using VideoGamesAPI.DataAccess.model;

/// <summary>
/// Repositorio que gestiona las operaciones relacionadas con los juegos.
/// </summary>
public class GameRepository : IGameRepository
{
    private readonly GameDbContext _dbContext;
    private readonly IMapper _mapper;

    public GameRepository(GameDbContext dbContext, IMapper mapper)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
    }

    public IEnumerable<GetGameDto> GetGames()
    {
        var games = _dbContext.Games.ToList();
        return _mapper.Map<List<GetGameDto>>(games);
    }

    public GetGameDto GetGame(Guid id)
    {
        var game = _dbContext.Games.FirstOrDefault(g => g.Id == id);
        return _mapper.Map<GetGameDto>(game);
    }

    public GetGameDto AddGame(GameDto gameDto)
    {
        var game = _mapper.Map<GameEntity>(gameDto);
        _dbContext.Games.Add(game);
        _dbContext.SaveChanges();
        return _mapper.Map<GetGameDto>(game);
    }

    public GetGameDto UpdateGame(Guid id, GameDto gameDto)
    {
        var existingGame = _dbContext.Games.FirstOrDefault(g => g.Id == id);
        existingGame.Name = gameDto.Name;
        existingGame.Platform = gameDto.Platform;
        existingGame.Price = gameDto.Price;

        _dbContext.SaveChanges();

        return _mapper.Map<GetGameDto>(existingGame);
    }

    public void DeleteGame(Guid id)
    {
        var game = _dbContext.Games.FirstOrDefault(g => g.Id == id);
        if (game != null)
        {
            _dbContext.Games.Remove(game);
            _dbContext.SaveChanges();
        }
    }

    public bool IsNameUnique(string name, Guid? excludeId = null)
    {
        return !_dbContext.Games.Any(g => g.Name == name && g.Id != excludeId);
    }
}