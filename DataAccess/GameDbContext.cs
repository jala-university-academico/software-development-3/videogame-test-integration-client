﻿namespace VideoGamesAPI.DataAccess;

using Microsoft.EntityFrameworkCore;
using VideoGamesAPI.DataAccess.model;

/// <summary>
/// Representa un DbContext para gestionar juegos.
/// </summary>
public class GameDbContext : DbContext
{
    /// <summary>
    /// Obtiene o establece el DbSet de entidades de GameEntity.
    /// </summary>
    public DbSet<GameEntity> Games { get; set; }
    public DbSet<UserEntity> Users { get; set; }

    /// <summary>
    /// Inicializa una nueva instancia de la clase GameDbContext con las opciones especificadas.
    /// </summary>
    /// <param name="options">Las opciones de DbContext.</param>
    public GameDbContext(DbContextOptions<GameDbContext> options) : base(options)
    {
    }
}
