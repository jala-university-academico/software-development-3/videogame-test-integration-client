﻿using AutoMapper;
using VideoGamesAPI.BusinessLogic.dto;
using VideoGamesAPI.DataAccess.model;

namespace VideoGamesAPI.DataAccess.mapper;

/// <summary>
/// Clase que proporciona mapeo entre las entidades de juego y los DTOs de juego.
/// </summary>
public class GameMapping : Profile
{
    /// <summary>
    /// Inicializa una nueva instancia de la clase <see cref="GameMapping"/>.
    /// Define los mapeos entre las entidades de juego y los DTOs de juego.
    /// </summary>
    public GameMapping()
    {
        CreateMap<GameEntity, GetGameDto>().ReverseMap();
        CreateMap<GameEntity, GameDto>().ReverseMap();
    }
}