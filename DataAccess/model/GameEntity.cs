﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VideoGamesAPI.DataAccess.model;

[Table("Games")]
[Index(nameof(Name), IsUnique = true)]
public class GameEntity
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid Id { get; set; }

    [Required]
    [StringLength(50)]
    public string Name { get; set; }

    [Required]
    [RegularExpression("^(Xbox|Play Station)$")]
    public string Platform { get; set; }

    [Required]
    public decimal Price { get; set; }
}